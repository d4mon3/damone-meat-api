import * as restify from 'restify';
import { Ouser } from '../../components/users/users.model';
import { NotAuthorizedError } from 'restify-errors';
import * as jwt from 'jsonwebtoken';
import { enviroment } from '../enviroments';

export const authenticate: restify.RequestHandler = (req, resp, next) => {
  const { email, password } = req.body;

  Ouser.findByEmail(email,'+password')
    .then(user => {
      if(user && user.matches(password)) {
        const token = jwt.sign({ sub: user.email.toString, iss: enviroment.appname },
          enviroment.security.apiSecret);
        resp.json({ name: user.name, email: user.email, accessToken: token });
        return next(false);
      } else {
        return next(new NotAuthorizedError('Invalid creditials'));
      }
    }).catch(next);
}