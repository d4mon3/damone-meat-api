import { Server } from './server/server';
import { usersRouter } from './components/users/users.routers';
import { restaurantsRouter } from './components/restaurants/restaurants.router';
import { reviewsRouter } from './components/reviews/reviews.routers';

const server = new Server()
server.bootstrap([
  usersRouter,
  restaurantsRouter,
  reviewsRouter,
]).then(server => {
  console.log('Server is listening on: ', server.application.address());
}).catch(error => {
  console.log('Server failed to start');
  console.log(error);
  process.exit(1);
});



