import * as mongoose from 'mongoose';
import { validateCPF } from '../../common/validators';
import * as bcrypt from 'bcrypt';
import { enviroment } from '../../common/enviroments';


export interface Ouser extends mongoose.Document{
  //_id: string,
  name: string,
  email: string,
  password: string,
  cpf: string,
  gender: string,
  profiles: string[],
  matches(password: string): boolean,
  hasAny(...profiles: string[]): boolean
}

export interface OuserModel extends mongoose.Model<Ouser>{
  findByEmail(email: string, projection?: string): Promise<Ouser>
}

const ouserSchema = new mongoose.Schema({
  name: { type: String, required:true, maxlength:50, minlength:4 },
  email: { type: String, unique: true , required:true, match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
  password: { type: String, select: false, required: true },
  gender: { type: String, required: false, enum: ['Male', 'Female'] },
  //cpf: { type: String, required: false, validate: { validator: validateCPF, message: '{PATH}: Invalid CPF ({VALUE})' } }
  profiles: { type: [String], required: false}


});

ouserSchema.statics.findByEmail = function (email: string, projection: string) {
  return this.findOne({email}, projection); //{email: email}
}

ouserSchema.methods.matches = function (password: string): boolean {
  return bcrypt.compareSync(password, this.password);
} 

ouserSchema.methods.hasAny = function(...profiles: string[]) : boolean {
  return profiles.some(profile => this.profiles.indexOf(profile) !== -1);
}

const hashPassword = (obj, next) => {
  bcrypt.hash(obj.password, enviroment.security.saltRounds)
    .then(hash => {
      obj.password = hash;
      next();
    }).catch(next);
}

const saveMiddleware = function (next) {
  const user: Ouser = this;
  if (!user.isModified('password')) {
    next();
  } else {
    hashPassword(user, next);
  }
}

const updateMiddleware = function (next) {
  if (!this.getUpdate().password) {
    next();
  } else {
    hashPassword(this.getUpdate(), next);
  }
}

ouserSchema.pre('save', saveMiddleware);
ouserSchema.pre('findOneAndUpdate', updateMiddleware);
ouserSchema.pre('update', updateMiddleware);

export const Ouser = mongoose.model<Ouser, OuserModel>('Ouser', ouserSchema);