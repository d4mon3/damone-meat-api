import * as mongoose from 'mongoose';

export interface MenuItemDocument extends mongoose.Document {
  name: string,
  price: number
};

export interface Restaurant extends mongoose.Document{
  name: string,
  endereco: string,
  nota: number,
  menu: MenuItemDocument[]
};

const menuSchema = new mongoose.Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true }
});

const restaurantSchema = new mongoose.Schema({
  name: { type: String, required: true },
  endereco: { type: String, required: true, maxlength: 100, minlength: 5 },
  nota: { type: Number, required: false, min: 1, max: 5, default: 1 },
  menu: { type: [menuSchema], required: false, select: false, default: [] }
});

export const Restaurant = mongoose.model<Restaurant>('Restaurant', restaurantSchema);