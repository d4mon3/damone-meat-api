import { ModelRouter } from '../../common/router-model';
import { Restaurant } from './restaurants.model';
import * as restify from 'restify';
import { NotFoundError } from 'restify-errors';
import { authorize } from '../../common/security/autz.handler';

class RestaurantsRouter extends ModelRouter<Restaurant> {
  constructor() {
    super(Restaurant)
  }

  envelope(document) {
    let resource = super.envelope(document);
    resource._links.menu = `${this.basePath}/${resource._id}/menu`;
    return resource;
  }



  findMenu = (req, resp, next) => {
    Restaurant.findById(req.params.id, "+menu")
      .then(rest => {
        if (!rest) {
          throw new NotFoundError('Restaurant not found - restaurants.router/findMenu');
        } else {
          resp.json(rest.menu);
          return next();
        }
      }).catch(next);
  }
  replaceMenu = (req, resp, next) => {
    Restaurant.findById(req.params.id).then(rest => {
      if (!rest) {
        throw new NotFoundError('Restaurant not found - restaurants.router/findMenu');
      } else {
        rest.menu = req.body; //array de menuItem
        return rest.save();
      }
    }).then(rest => {
      resp.json(rest.menu);
      return next();
    }).catch(next);
  }

  applyRoutes(app: restify.Server) {
    app.get(`${this.basePath}`, this.findAll);
    app.get(`${this.basePath}/:id`, this.findById);
    app.post(`${this.basePath}`, [authorize('admin'), this.save]);
    app.put(`${this.basePath}/:id`, [authorize('admin'), this.replace]);
    app.patch(`${this.basePath}/:id`, [authorize('admin'), this.update]);
    app.del(`${this.basePath}/:id`, [authorize('admin'), this.delete]);

    app.get(`${this.basePath}/:id/menu`, this.findMenu);
    app.put(`${this.basePath}/:id/menu`, this.replaceMenu);
  }
}

export const restaurantsRouter = new RestaurantsRouter();

