import { ModelRouter } from '../../common/router-model';
import { Review } from './reviews.model';
import * as restify from 'restify';
import * as mongoose from 'mongoose';
import { authorize } from '../../common/security/autz.handler';


class ReviewsRouter extends ModelRouter<Review>{
  constructor() {
    super(Review);
  }

  protected prepareOne(query: mongoose.DocumentQuery<Review, Review>): mongoose.DocumentQuery<Review, Review>{
    return query.populate('user', 'name')
      .populate('restaurant', ['name', 'endereco']);
  }

  envelope(document) {
    let resource = super.envelope(document);
    const restId = document.restaurant._id ? document.restaurant._id : document.restaurant;
    resource._links.restaurant = `/restaurants/${restId}`;
    return resource;
  }

  /*findById = (req, res, next) => {
    this.model.findById(req.params.id)
      .populate('restaurant', ['name', 'endereco'])
      .populate('user', 'name')
      .then(this.render(res, next))
      .catch(next);
  }*/

  applyRoutes(app: restify.Server) {
    
    app.get(`${this.basePath}`, this.findAll);
    app.get(`${this.basePath}/:id`, this.findById);
    app.post(`${this.basePath}`, [authorize('user'), this.save]);
  }
}

export const reviewsRouter = new ReviewsRouter();